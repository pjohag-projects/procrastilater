import { getBlockedUrls } from './storage';

// Prohibit user from viewing content of tab
async function blockTab(tab: browser.tabs.Tab) {
    const code = `document.body.innerHTML = '<h1>Procrastinate Later!</h1>';`;
    return browser.tabs.executeScript(tab.id, { code });
}


// Block already opened tabs in regular intervals
async function run() {
    while (true) {
        console.log("Checking to block");
        const blockList = await getBlockedUrls(true);
        const matchUrls = blockList.map((b) => b.url + "*")
        console.log("Matching URLs");
        const tabs = await browser.tabs.query({ url: matchUrls });
        console.log("Will block", tabs);
        tabs.forEach((t) => blockTab(t));

        await new Promise(resolve => setTimeout(resolve, 10 * 1000));
    }
}

// Block newly opened tabs immediately
async function redirect(requestDetails: { url: string }) {
    const blockList = await getBlockedUrls(true);
    const shouldBlock = blockList.find((b) => b.url == requestDetails.url);
    if (shouldBlock) {
        return {
            redirectUrl: browser.extension.getURL("./blockpage.html")
        };
    }
}

browser.webRequest.onBeforeRequest.addListener(
    redirect,
    { urls: ["<all_urls>"] },
    ["blocking"]
);

run();
