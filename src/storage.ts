// Fetch blocked URLs from persistent storage
export async function getBlockedUrls(onlyActive = false): Promise<BlockEntry[]> {

    function isActive(entry: BlockEntry) {
        const now = new Date();
        const from = new Date(now);
        const to = new Date(now);
        from.setHours(entry.from[0], entry.from[1]);
        to.setHours(entry.to[0], entry.to[1]);
        return (now >= from && now <= to);
    }

    const blockedUrlsObject = (await browser.storage.local.get("blockedUrls")) as StoredBlockList;
    const blockList = blockedUrlsObject.blockedUrls || [];
    return onlyActive ? blockList.filter(isActive) : blockList;
}


export async function addBlockedUrl(blockEntry: BlockEntry) {
    const blockedUrls = await getBlockedUrls();
    blockedUrls.push(blockEntry);
    browser.storage.local.set({ "blockedUrls": blockedUrls });
}

export async function replaceBlockedUrls(blockEntries: BlockEntry[]) {
    browser.storage.local.set({ "blockedUrls": blockEntries });
}



