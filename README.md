**ProcrastiLater** is a [Firefox Extension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions) which allows you to block websites during specific times of the day in order to increase productivity.
You can get the extension by downloading the latest 
[artifacts](https://gitlab.com/pjohag-projects/procrastilater/-/pipelines?page=1&scope=all&ref=master&status=success)
or you can build it yourself by running `npm install` and `npm run build`.

Then, in Firefox: 
1. Visit the `about:debugging` page
2. In the menu on the left, click "This Firefox"
3. Click "Load Temporary Add-on"
4. Select any file in the directory `dist/` in the repository (or the downloaded artifacts).

Now a button for ProcrastiLater will be added to your toolbar.
Clicking the button will open a popup where you can enter the URLs of potentially distracting websites
(e.g. `www.reddit.com/`) and the timeframes in which they will be blocked.
